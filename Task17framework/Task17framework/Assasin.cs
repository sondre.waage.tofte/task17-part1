﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17framework
{

    public class Assasin : Thief
    {
        //Setting and getting all the values
        private string name;
        private int hP;
        private int mana;
        private int armor;

        public string Name { get => name; set => name = value; }
        public int HP { get => hP; set => hP = value; }
        public int Mana { get => mana; set => mana = value; }
        public int Armor { get => armor; set => armor = value; }

        public Assasin(string name, int hP, int mana, int armor)
        {
            Name = name;
            HP = hP;
            Mana = mana;
            Armor = armor;
        }
    }
}
