﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17framework
{
    class Program
    {
        static void Main(string[] args)
        {
            FireMage Unternehmer = new FireMage("Unternehmer", 1000, 200, 10);
            Fighter SteinSterk = new Fighter("Steinsterk", 1500, 100, 20);
            Assasin Donger = new Assasin("Donger", 1500, 150, 10);
            Console.WriteLine(Donger.Name);
            Console.WriteLine(Unternehmer.Mana);
            Console.WriteLine(SteinSterk.HP);
            Console.ReadKey();
        }
    }
}
