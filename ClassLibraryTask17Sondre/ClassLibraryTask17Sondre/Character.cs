﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryTask17Sondre
{
    public class Character
    {

        //Setting and getting all the values
        private string name;
        private int hP;
        private int mana;
        private int armor;

        public string Name { get => name; set => name = value; }
        public int HP { get => hP; set => hP = value; }
        public int Mana { get => mana; set => mana = value; }
        public int Armor { get => armor; set => armor = value; }

        public Character(string name, int hP, int mana, int armor)
        {
            Name = name;
            HP = hP;
            Mana = mana;
            Armor = armor;
        }
        public string GetInfo(string name)
        {
            string info = "no info";
            return info;
        }

        public void Attack()
        {

        }
        public void Move()
        {

        }
    }
}
